#!/bin/bash
#author Bernadin Tonoudji
#to add multiple account mail giving by a file in vestacp
#the file must contain only the first part of the address mail
# if you want to create ben@coco.bj you need to have in a line of your file coco
#be sure you have no space in the file

FILE=$1
ANOTHER_FILE=$2
USERNAME=$3
DOMAIN=$4
DEFAULTPASSWORD=$5
QUOTA=$6

exec 4> "$ANOTHER_FILE"

while IFS= read -ru 3 LINE; do
    v-add-mail-account $USERNAME $DOMAIN "$LINE" $DEFAULTPASSWORD $QUOTA >&4
done 3< "$FILE"
