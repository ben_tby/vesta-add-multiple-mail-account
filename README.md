
#author Bernadin Tonoudji : bernadintonoudji@gmail.com

If you have installed VestaCP on your server and need to add multiple email addresses simultaneously, you agree that it is complicated to do it one by one manually.
You must add x email addresses on a domain name, you just have to: 
 to create the domain name on your VestaCP account,
list the addresses in a file in this format: (no space on the lines)
       coconut
       ablo
       test
you just have to execute the script on the server. It takes 6 arguments in this precise order: 

1. the file to be processed
2. the output file
3. the name of the user who owns the domain name
4. the domain name
5. the default password for all accounts
6. the default quota for all accounts. 

Example of use: 
./add-account.sh input.txt output.txt output.txt ben toto.com Strongpasswordin88 2048


 